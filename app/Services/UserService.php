<?php

namespace App\Services;
use App\Contracts\UserInterface;
use App\User;
use Auth;

class UserService implements UserInterface
{
	/**
     * Create a new instance of UserService.
     *
     * @return void
     */
	function __construct()
	{
		$this->user = new User();
	}

    /**
     * get user
     *
     * @param integer $id
     * @return user
     */
    public function getAuthUser()
    {
        return Auth::user();
    }

	/**
     * get user
     *
     * @param integer $id
     * @return user
     */
	public function getOne($id)
	{
		return $this->user->find($id);
	}

	/**
     * update user
     *
     * @param integer $id
     * @param integer $param
     * @return bool
     */
	public function update($id, $param)
    {
        return $this->getOne($id)->update($param);
    }

	/**
      * create user 
      *
      * @param array $params
      * @return response
      */ 
	public function add($params)
	{
        $user = $this->user->create($params);
        return $user;
	}

    /**
     * Delete user.
     *
     * @return bool
     */
    public function deleteOne($id)
    {
    	$user = $this->user->find($id)->delete();
    	return $user;
    }

}