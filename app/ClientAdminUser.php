<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientAdminUser extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_admin_id', 'user_id'
    ];
}
