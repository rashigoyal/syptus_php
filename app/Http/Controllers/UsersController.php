<?php

namespace App\Http\Controllers;

use App\Contracts\UsersInterface;

use App\Http\Requests;
use App\Http\Requests\LoginRequest;

use Auth;

class UsersController extends Controller
{
    /**
    * Create a new instance of UsersController class.
    *
    * @return void
    */
    public function __construct()
    {
       
    }

    /**
    * Render view for logging in the application.
    * GET /login
    *
    * @return view
    */
    public function getIndex()
    {
        return view('login');
    }

    public function postIndex(LoginRequest $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        if(Auth::attempt([
                'email' => $email,
                'password' => $password,
                'role' => 'client_admin'
            ]))
        {
            return redirect()->action('UsersController@getDashboard');
        }else{
            return redirect()->back()->with('error_danger', 'The password or email you’ve entered is incorrect');
        }
    }

    public function getDashboard()
    {
        return view('dashboard');
    }

    public function getAddClient()
    {

    }

    /**
     * user log out
     * GET user/logout
     * 
     * @return redirect()
     */
    public function getLogout()
    {
        Auth::logout();
        return redirect()->action('UsersController@getIndex');
    }
}
      
  