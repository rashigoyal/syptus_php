<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'email'=>'required|email',
        ];
    }

    /**
     * Get all inputs
     *
     * @return array
     */
    public function inputs()
    {
        $inputs = $this->all();
        $inputs['password'] = bcrypt($inputs['password']);
        return $inputs;
    }
}
