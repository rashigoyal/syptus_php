<?php namespace App\Contracts;
	
interface UserInterface
{
	/**
     * get user
     *
     * @param integer $id
     * @return user
     */
	public function getOne($id);

    /**
     * get user
     *
     * @param integer $id
     * @return user
     */
    public function add($data);

	/**
     * update user
     *
     * @param integer $id
     * @param integer $param
     * @return bool
     */
	public function update($id,$param);

    /**
     * Delete user.
     *
     * @param integer $id
     * @return bool
     */
    public function deleteOne($id);
}