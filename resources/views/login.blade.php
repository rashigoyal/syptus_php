<!DOCTYPE html>
<html lang="en">
    <head>
    	<title>Alpha</title>
    	<meta charset="utf-8">
    	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    	<!-- Bootstrap 3.3.5 -->
        {!! HTML::style( asset('assets/css/bootstrap.min.css')) !!}
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        {!! HTML::style( asset('assets/dist/css/AdminLTE.min.css')) !!}
        <!-- iCheck -->
        {!! HTML::style( asset('assets/plugins/iCheck/square/blue.css')) !!}
    </head>

    <body class='hold-transition login-page'>
        <div id="main">
                <div class="login-box">
                    <div class="login-logo">
                        <a href="#"><b>Alpha</b></a>
                    </div>
                    @include('message')
                    <div class="login-box-body">
                        <p class="login-box-msg">Sign in to start your session</p>
                        {!! Form::open(['action' => ['UsersController@postIndex'], 'class' => 'form-horizontal', 'role' => 'form', 'files' => 'true','method' =>'POST' ]) !!}
                            <div class="form-group has-feedback">
                                {!! Form::text('email',null,['placeholder' => 'Email','class'=>'form-control']) !!}
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                {!! Form::password('password',['placeholder' => 'Password','class'=>"form-control"]) !!}
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="row" style='margin-left:0'>
                                <div class="col-xs-8">
                                </div><!-- /.col -->
                                <div class="col-xs-4">
                                    <button  type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                                </div><!-- /.col -->
                            </div>
                        {!! Form::close() !!}   
                            
                    </div><!-- /.login-box-body -->
                </div><!-- /.login-box -->       
        </div>

        <!-- jQuery 2.1.4 -->
        {!! HTML::script( asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') ) !!}
        <!-- Bootstrap 3.3.5 -->
        {!! HTML::script( asset('assets/js/bootstrap.min.js') ) !!}
        <!-- iCheck -->
        {!! HTML::script( asset('assets/plugins/iCheck/icheck.min.js') ) !!}
    </body>
</html>