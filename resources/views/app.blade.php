<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Alpha</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.5 -->
    {!! HTML::style( asset('assets/css/bootstrap.min.css')) !!}
    <!-- Font Awesome -->
    {!! HTML::style( asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css')) !!}
    <!-- Ionicons -->
    {!! HTML::style( asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')) !!}
     <!-- Theme style -->
    {!! HTML::style( asset('assets/dist/css/AdminLTE.min.css')) !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    {!! HTML::style( asset('assets/dist/css/skins/_all-skins.min.css')) !!}
    <!-- iCheck -->
    {!! HTML::style( asset('assets/plugins/iCheck/flat/blue.css')) !!}
    <!-- Morris chart -->
    {!! HTML::style( asset('assets/plugins/morris/morris.css')) !!}
    <!-- jvectormap -->
    {!! HTML::style( asset('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css')) !!}
    <!-- Date Picker -->
    {!! HTML::style( asset('assets/plugins/datepicker/datepicker3.css')) !!}
    <!-- Daterange picker -->
    {!! HTML::style( asset('assets/plugins/daterangepicker/daterangepicker-bs3.css')) !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! HTML::style( asset('assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')) !!}


 @yield('styles')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    @yield('content')

	<!-- jQuery 2.1.4 -->
	{!! HTML::script( asset('assets/plugins/jQuery/jQuery-2.1.4.min.js') ) !!}

    <!-- Bootstrap 3.3.5 -->
    {!! HTML::script( asset('assets/js/bootstrap.min.js') ) !!}
    
    <!-- Slimscroll -->
    {!! HTML::script( asset('assets/plugins/slimScroll/jquery.slimscroll.min.js') ) !!}
    <!-- FastClick -->
    {!! HTML::script( asset('assets/plugins/fastclick/fastclick.min.js') ) !!}
    <!-- AdminLTE App -->
    {!! HTML::script( asset('assets/dist/js/app.min.js') ) !!}
    <!-- AdminLTE for demo purposes -->
    {!! HTML::script( asset('assets/dist/js/demo.js') ) !!}

	@yield('scripts')
</body>
</html>
