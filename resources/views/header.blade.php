<header class="main-header">
				<!-- Logo -->
				<a href="#" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>AL</b></span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Alpha Connect</b></span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top" role="navigation">
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<span class="hidden-xs">Admin</span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="{{ asset('assets/dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-right">
											<a href="{{action('UsersController@getLogout')}}" class="btn btn-default btn-flat">Log out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu">  
			            @if(isset($activeUsers))
			                <li class="active treeview">
			            @else
			                <li class="treeview">
			            @endif
			                    <a href="#">
			                        <i class="fa fa-circle"></i> <span>Clients</span> <i class="fa fa-angle-left pull-right"></i>
			                    </a>
			                    <ul class="treeview-menu">
			                        @if(isset($activeAddClient))
			                            <li class="active">
			                        @else
			                            <li>
			                        @endif
			                                <a href="{{action('UsersController@getAddClient')}}"><i class="fa fa-circle-o"></i>Add Client</a>
			                            </li>
			                    </ul>
			                </li>

			   
					</ul>
				</section>
		<!-- /.sidebar -->
		</aside>