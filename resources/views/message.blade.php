@if (count($errors) != 0)
<div class="alert alert-danger">
    @foreach ($errors as $error)
        {{ $error }}<BR>       
    @endforeach
</div>
@endif

@if(Session::has('success'))
<div class="alert alert-success">
 {{Session::get('success')}}
</div> 
@endif

@if(Session::has('error_danger'))
<div class="alert alert-danger">
 {{Session::get('error_danger')}}
</div>
@endif