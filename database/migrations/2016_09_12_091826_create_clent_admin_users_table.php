<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClentAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clent_admin_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_admin_id');
            $table->integer('user_id');
            $table->enum('role', ['client_admin', 'user'])->default('user');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clent_admin_users');
    }
}
