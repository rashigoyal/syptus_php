<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert(['name' => 'client_admin', 'email' => 'client_admin@mail.ru', 'role' => 'client_admin', 'password' => bcrypt('admin')]);
    }
}
